from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.redirect_view ,name="home"),
    path('show_facts',views.show_facts,name="show_facts")
]