from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

# Create your views here.

def home(request):
    return HttpResponse('Hello World');

def show_facts(request):
    return HttpResponse('The sun rises in the east');

def redirect_view(request):
    response = redirect('show_facts')
    return response;