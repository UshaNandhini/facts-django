# Facts-Django


## To create a Virtual Environment and to activate it

- virtualenv env_name -p python3 
- source env_name/bin/activate


## Installing Django

- pip install django
- python -m django --version


## Create Django simple Project

- django-admin startproject example_project

## Migrate and Run Server

- python manage.py migrate
- python manage.py runserver


## Create App

- python manage.py startapp facts

## Migrating Models

- python manage.py makemigrations facts
- python manage.py sqlmigrate facts 0001
- python manage.py migrate


## Creating an admin User

- python manage.py createsuperuser


